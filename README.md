# Grew daemon

`grew_daemon` is an executable daemon from the **Grew-match** application.

See:

  * [https://grew.fr/grew_match/help](https://grew.fr/grew_match/help) for documentation about **Grew-match** usage
  * [https://grew.fr/grew_match/install](https://grew.fr/grew_match/install) for a local installation of **Grew-match**
