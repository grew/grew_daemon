open Printf

(* Run a command and return its results as a list of strings,
   one per line. *)
let read_process_lines command =
  let lines = ref [] in
  let in_channel = Unix.open_process_in command in
  begin
    try
      while true do
        lines := input_line in_channel :: !lines
      done;
    with End_of_file ->
      ignore (Unix.close_process_in in_channel)
  end;
  List.rev !lines


let uuid = match (read_process_lines "uuidgen") with
	| [one] -> one
	| _ -> failwith "Unexpected output for comman uuidgen"

let port = ref 8080

let pattern = String.concat "\n" [
  "match { N[lemma=penser] }"
  ]

let _ =
  let rec parse = function
  | [] -> ()
  | "-port" :: n :: tail -> port := int_of_string n; parse tail
  | s::_ -> failwith (Printf.sprintf "Don't know what to do with arg >>%s<<" s) in
  match Array.to_list Sys.argv with
  | [] -> assert false
  | _::args -> parse args

let _ =
  let sockaddr = Unix.ADDR_INET (Unix.inet_addr_loopback,!port) in

  let dir = sprintf "/tmp/%s" uuid in
  let _ = Sys.command (sprintf "mkdir %s" dir) in

  let out_ch = open_out (Filename.concat dir "pattern") in
  fprintf out_ch "%s" pattern;
  close_out out_ch;



  let command  = ref "NEW" in

  while true do
    let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in

    let (ic,oc) =
  	try
	  Unix.connect sock sockaddr;
	  (Unix.in_channel_of_descr sock, Unix.out_channel_of_descr sock)
	with exn -> Unix.close sock; raise exn in

    (* send request *)
    output_string oc (sprintf "/tmp/%s#%s" uuid !command);
    flush oc;

    (* get reply *)
    let reply = input_line ic in
    Printf.printf "reply=%s\n%!" reply;

    Unix.close sock;

    let _ = read_line () in
    command := "NEXT"
  done;
