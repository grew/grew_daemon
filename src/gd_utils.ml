open Printf
open Log
open Libamr

exception Already_connected
exception Not_connected
exception Dep2pict_error of (string * string)
exception Unknown_corpus of string
exception Unknown_sent_id of string

(* ==================================================================================================== *)
module String_map = Map.Make (String)

(* ==================================================================================================== *)
module List_ = struct
  let foldi_right f l init =
    fst
      (List.fold_right
         (fun elt (acc,i) -> (f i elt acc, i-1))
         l (init,(List.length l) - 1)
      )

  let find_index fct l =
    let rec loop i = function
      | [] -> raise Not_found
      | x::_ when fct x -> (i,x)
      | _::t -> loop (i+1) t in
    loop 0 l
end

(* ==================================================================================================== *)
module Array_ = struct
  let foldi_left f x a =
    let r = ref x in
    for i = 0 to Array.length a - 1 do
      r := f i !r a.(i)
    done;
    !r

  let shuffle_N n =
    Random.self_init ();
    let a = Array.init n (fun x -> x) in
    for i = n-1 downto 1 do
      let j = Random.int (i+1) in
      let tmp = a.(i) in
      a.(i) <- a.(j);
      a.(j) <- tmp
    done;
    a;;
end


(* ==================================================================================================== *)
module Utils = struct
  let sock_recv sock maxlen =
    let str = Bytes.create maxlen in
    let recvlen = Unix.recv sock str 0 maxlen [] in
    Bytes.sub str 0 recvlen

  let send_string sock str =
    let len = Bytes.length str in
    let _ = Unix.send sock str 0 len [] in
    ()

  let fill dir filename text =
    let file = Filename.concat dir filename in
    let out_ch = open_out_gen [Open_creat; Open_append] 0o644 file in
    fprintf out_ch "%s\n" text;
    close_out out_ch

  let fill_list dir text = fill dir "list" text

  (* fill [text] to the file "error" in [dir] *)
  let error dir text =
    let error_file = Filename.concat dir "error" in
    let out_ch = open_out error_file in
    fprintf out_ch "%s\n" text;
    close_out out_ch

  let read_file file =
    let in_ch = open_in file in
    let rev_lines = ref [] in
    try
      while true do
        let line = input_line in_ch in
        (* empty lines and lines starting with % are ignored *)
        if (Str.string_match (Str.regexp "^[ \t]*$") line 0) || (line.[0] = '%')
        then ()
        else rev_lines := line :: !rev_lines
      done; assert false
    with End_of_file ->
      close_in in_ch;
      List.rev !rev_lines

  let read_first_line file =
    let in_ch = open_in file in
    try input_line in_ch
    with End_of_file -> ""

  let print_with_lines s =
    let line = String.make (String.length s) '-' in
    printf "%s\n%s\n%s\n" line s line

  let read_dir extensions directory =
    Array.fold_left
      (fun acc file ->
         if List.mem (Filename.extension file) extensions
         then file::acc
         else acc
      ) [] (Sys.readdir directory)

end (* module Utils *)

