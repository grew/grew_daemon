open Libgrew

module Request : sig
  val handle: Unix.file_descr -> string -> unit
end (* module Request *)
