open Printf
open Dep2pict
open Conllx
open Libgrew

open Gd_utils
open Gd_utils
open Gd_global
open Gd_types

(* ==================================================================================================== *)
(* If the same user requests for several patterns there will be several different [Request] for the daemon *)
module Request = struct
  open Session

  module String_opt_map = Map.Make (struct type t = string option let compare = compare end)

  let lemma = ref false
  let upos = ref false
  let upos = ref false
  let xpos = ref false
  let features = ref false
  let tf_wf = ref false
  let context = ref false

  let filter = function
    | "phon" | "form" -> true
    | "lemma" -> !lemma
    | "upos" | "cat" -> !upos
    | "xpos" | "pos" -> !xpos
    | "textform" | "wordform" -> !tf_wf
    | "SpaceAfter" -> false
    | s -> !features

  let save_dep uuid ~config ?audio_info rtl server_file base sent_id deco graph sentence meta =
    let dep = Graph.to_dep ~filter ~deco ~config graph in
    let filename = sprintf "%s.svg" base in
    let d2p =
      try Dep2pict.from_dep ~rtl ~dep
      with Dep2pict.Error json ->
        let message =
          match json |> Yojson.Basic.Util.member "message" |> Yojson.Basic.Util.to_string_option with
          | None -> Yojson.Basic.to_string json
          | Some s -> s in
        raise (Dep2pict_error (uuid, sprintf "[sent_id=%s] %s" sent_id message)) in
    let _ = Dep2pict.save_svg ~filename:(Filename.concat uuid filename) d2p in
    let shift = Dep2pict.highlight_shift () in
    let data = `Assoc (CCList.filter_map CCFun.id [
        Some ("kind", `String "ITEM");
        Some ("filename", `String filename);
        Some ("sent_id", `String sent_id);
        Some ("sentence", `String sentence);
        Some ("meta", `Assoc (List.map (fun (k,v) -> (k, `String v)) meta));
        CCOpt.map (fun v -> ("shift", `Float v)) shift;
        CCOpt.map (fun s -> ("audio", `String s)) audio_info;
      ]) in
    Utils.fill uuid server_file (sprintf "%s" (Yojson.Basic.to_string data));
    ()

  let save_dot uuid ~config server_file base sent_id deco graph sentence meta code_opt =
    let dot = Graph.to_dot ~deco ~config graph in
    let temp_file_name,out_ch = Filename.open_temp_file ~mode:[Open_rdonly;Open_wronly;Open_text] "grew_" ".dot" in
    fprintf out_ch "%s" dot;
    close_out out_ch;
    let filename = sprintf "%s.svg" base in
    ignore (Sys.command(sprintf "dot -Tsvg -o %s %s " (Filename.concat uuid filename) temp_file_name));
    let data = `Assoc [
        ("kind", `String "ITEM");
        ("filename", `String filename);
        ("sent_id", `String sent_id);
        ("sentence", `String sentence);
        ("meta", `Assoc (List.map (fun (k,v) -> (k, `String v)) meta));
        ("code", match code_opt with Some s -> `String s | None -> `Null);
      ] in
    Utils.fill uuid server_file (sprintf "%s" (Yojson.Basic.to_string data));
    ()

  let error_handle uuid exc =
    match exc with
    | Table.No_marshal_file file -> Utils.error uuid (sprintf "Unable to load corpus \"%s\"" file)
    | Already_connected -> Utils.error uuid "Already_connected"
    | Not_connected -> Utils.error uuid "Not connected"
    | Unknown_corpus corpus_name -> Utils.error uuid ("Unknown corpus: " ^ corpus_name ^ "\nTry to do a clean reload of the page!")
    | Dep2pict_error (uuid, msg) -> Utils.error uuid ("Dep2pict_error: " ^ msg)
    | Conllx_error js -> Utils.error uuid ("Conllx_error, please report with code: " ^ uuid)
    | Libgrew.Error msg -> Utils.error uuid msg
    | exc -> Utils.error uuid (sprintf "Unexpected error, please report [%s]" (Printexc.to_string exc))


  (* produce 10 more solutions *)
  let next uuid index =
    try
      let session = String_map.find uuid !Client_map.t in
      let config = Corpus_desc.get_config session.corpus_desc in
      let audio = Corpus_desc.is_audio session.corpus_desc in
      let rtl = Corpus_desc.is_rtl session.corpus_desc in
      let sub_result = session.results.(index) in
      let server_file = sprintf "cluster_%d" index in
      let rec loop = function
        | ({data; next},_) when (Array.length data) = next ->
          let json = `Assoc [ ("kind", `String "END") ] in
          Utils.fill uuid server_file (sprintf "%s" (Yojson.Basic.to_string json));
          next
        | ({next}, 0) ->
          let json = `Assoc [ ("kind", `String "PAUSE") ] in
          Utils.fill uuid server_file (sprintf "%s" (Yojson.Basic.to_string json));
          next
        | ({data; next} as sr, n) ->
          let occ = data.(next) in
          let corpus = Table.get_index session.index in
          begin
            let deco = Deco.build session.pattern occ.matching in
            let pos = occ.corpus_index in
            let graph = Corpus.get_graph pos corpus in

            let (sentence, audio_bounds) =
              match (audio, Corpus.is_conll corpus) with
              | (true, _) -> Graph.to_orfeo ~deco graph
              | (false, true) -> ((match Graph.to_sentence ~deco graph with "" -> Corpus.get_text pos corpus | x -> x), None)
              | (false, false) -> (Corpus.get_text pos corpus, None) in

            let sent_with_context =
              if !context
              then
                let prev_sent = try (Corpus.get_text (pos-1) corpus)^"</br>" with _ -> "" in
                let next_sent = try "</br>"^(Corpus.get_text (pos+1) corpus) with _ -> "" in
                sprintf "%s <font color=\"#FC5235\">%s</font> %s" prev_sent sentence next_sent
              else sprintf "<font color=\"#FC5235\">%s</font>" sentence in

            let meta_list =
              List.filter
                (function
                  | ("text",_) | ("sent_id",_) | ("wav_url",_) -> false
                  | (s,_) when String.length s > 2 && String.sub s 0 2 = "##" -> false
                  | _ -> true
                ) (Graph.get_meta_list graph) in
            match Corpus.is_conll corpus with
            | true ->
              let audio_info =
                match (audio, Graph.get_meta_opt "wav_url" graph, audio_bounds) with
                | (true, Some url, Some (i,f)) -> Some (sprintf "%s#t=%g,%g" url i f)
                | _ -> None in
              save_dep uuid ~config ?audio_info rtl server_file occ.filename occ.list_item deco graph sent_with_context meta_list
            | false ->
              save_dot uuid ~config server_file occ.filename occ.list_item deco graph sent_with_context meta_list (Graph.get_code_opt graph)
          end;
          loop ({sr with next = sr.next + 1}, n-1) in
      let next = loop (sub_result, Global.nbre_sol_page) in
      session.results.(index) <- { sub_result with next};
      Client_map.t :=
        String_map.add uuid
          { session with last=Unix.gettimeofday () }
          !Client_map.t;
    with Not_found -> raise Not_connected

  let write_clusters uuid =
    let request = String_map.find uuid !Client_map.t in
    Array.iteri
      (fun index sub_result ->
         Utils.fill uuid "clusters"
           (sprintf "<CLUSTER>@@%s@@%d@@%d" (match sub_result.key with Some v -> v | None -> "undefined") sub_result.size index)
      ) request.results

  exception Max_results of occurrence list
  exception Max_time of occurrence list
  let add_new uuid =
    let start_time = Unix.gettimeofday () in
    let parameters =
      match Yojson.Basic.from_file (Filename.concat uuid "infos.json") with
      | `Assoc l ->
        List.map
          (fun (k,jv) -> match jv with
             | `String v -> (k,v)
             | _ -> failwith "Illegal format of the JSON \"infos\" file"
          ) l
      | _ -> failwith "Illegal format of the JSON \"infos\" file" in
    let corpus_name =
      match (List.assoc "corpus" parameters, List.assoc_opt "eud2ud" parameters) with
      | (name, Some "true") -> "ud__" ^ name
      | (name, _) -> name in
    lemma := (List.assoc_opt "lemma" parameters = Some "true");
    upos := (List.assoc_opt "upos" parameters = Some "true");
    xpos := (List.assoc_opt "xpos" parameters = Some "true");
    features := (List.assoc_opt "features" parameters = Some "true");
    tf_wf := (List.assoc_opt "tf_wf" parameters = Some "true");
    context := (List.assoc_opt "context" parameters = Some "true");
    let (index, corpus, corpus_desc) = Table.get_name corpus_name in
    let config = Corpus_desc.get_config corpus_desc in
    let pattern = Pattern.parse ~config (List.assoc "pattern" parameters) in
    let ratio = ref 0. in (* percent of corpus scanned when there are too much solutions. *)
    let count = ref 0 in
    let len = Corpus.size corpus in
    let permut_fct =
      match List.assoc "order" parameters with
      | "shuffle" -> let mix = Array_.shuffle_N len in fun x -> mix.(x)
      | "length" -> let perm = Corpus.permut_length corpus in fun x -> perm.(x)
      | _ -> fun x -> x in

    let all_matchings =
      try
        List.rev (
          Array_.foldi_left
            (fun pos acc _ ->
               let graph = Corpus.get_graph (permut_fct(pos)) corpus in
               if Unix.gettimeofday() -. start_time > Global.max_time then (ratio := (100. *. (float pos) /. (float len)); raise (Max_time acc));
               let sent_id = Corpus.get_sent_id (permut_fct(pos)) corpus in
               if !count > Global.max_results
               then (ratio := (100. *. (float pos) /. (float len)); raise (Max_results acc))
               else
                 match Graph.search_pattern ~config pattern graph with
                 | [] -> acc
                 | [one] -> incr count; {corpus_index=permut_fct(pos); sent_id; filename=sprintf "%d" pos; list_item=sent_id; matching=one} :: acc
                 | several ->
                   let len = List.length several in
                   count := !count + len;
                   List_.foldi_right
                     (fun i matching acc2 ->
                        {corpus_index=permut_fct(pos); sent_id; filename=sprintf "%d_%d" pos i; list_item=sprintf "%s [%d/%d]" sent_id (len-i) len; matching} :: acc2
                     ) several acc
            ) [] (Array.make len ())
        )
      with 
      | Max_results res_list -> CCList.take Global.max_results (List.rev res_list) 
      | Max_time res_list -> CCList.take Global.max_results (List.rev res_list) 
      in

    match all_matchings with
    | [] ->
      let time = Unix.gettimeofday () -. start_time in
      Utils.fill_list uuid (sprintf "<EMPTY>@@%.2f" time)
    | _ ->
      let results =
        match (List.assoc_opt "clust1-type" parameters, List.assoc_opt "clust1-data" parameters)  with
        | (Some "key", Some key) ->
          let occ_list_map =
            List.fold_right
              (fun occurrence acc ->
                 let graph = Corpus.get_graph occurrence.corpus_index corpus in
                 let v = Matching.get_value_opt ~config key pattern graph occurrence.matching in
                 let old = try String_opt_map.find v acc with Not_found -> [] in
                 String_opt_map.add v (occurrence :: old) acc
              ) all_matchings String_opt_map.empty in
          Array.of_list (
            String_opt_map.fold
              (fun key occ_list acc ->
                 let data = Array.of_list occ_list in
                 {size=Array.length data; data; next=0; key} :: acc
              ) occ_list_map []
          )
        | (Some "whether", Some ext) ->
          let basic = Pattern.parse_basic ~config pattern ext in
          let (yes,no) =
            List.partition
              (fun occurrence ->
                 let graph = Corpus.get_graph occurrence.corpus_index corpus in
                 Matching.whether ~config basic pattern graph occurrence.matching
              ) all_matchings in
          let yes_array = Array.of_list yes in
          let no_array = Array.of_list no in
          [|
            {size=Array.length yes_array; data=yes_array; next=0; key=Some "Yes"};
            {size=Array.length no_array; data=no_array; next=0; key=Some "No"};
          |]
        | _ ->
          let data = Array.of_list all_matchings in
          [| {size=Array.length data; data; next=0; key=None} |] in

      Array.sort (fun sr1 sr2 -> compare sr2.size sr1.size) results;
      Client_map.t :=
        String_map.add uuid
          {
            index;
            pattern;
            last=Unix.gettimeofday ();
            results;
            corpus_desc;
          }
          !Client_map.t;
      let time = Unix.gettimeofday () -. start_time in
      if !count > Global.max_results
      then Utils.fill_list uuid (sprintf "<OVER>@@%.2f@@%.2f" !ratio time)
      else 
        if Unix.gettimeofday() -. start_time > Global.max_time
        then Utils.fill_list uuid (sprintf "<TIME>@@%d@@%.2f" (List.length all_matchings) !ratio)
        else Utils.fill_list uuid (sprintf "<TOTAL>@@%d@@%.2f" (List.length all_matchings) time);
      begin
        match Pattern.pid_name_list pattern with
        | [] -> Utils.fill_list uuid "<PIVOTS>"
        | l -> Utils.fill_list uuid (sprintf "<PIVOTS>@@%s" (String.concat "@@" l))
      end;
      match List.assoc_opt "clust1-type" parameters with
      | Some "key" | Some "whether" -> Utils.fill_list uuid "<CLUSTERS>"; write_clusters uuid
      | _ -> Utils.fill_list uuid "<ONECLUSTER>"; next uuid 0

  let export ?pivot uuid =
    let session =
      try String_map.find uuid !Client_map.t
      with Not_found -> raise Not_connected in
    let corpus = Table.get_index session.Session.index in
    let filename = Filename.concat uuid "export.tsv" in
    let out_ch = open_out filename in
    begin
      match pivot with
      | None -> fprintf out_ch "sent_id\tsentence\n"
      | Some _ -> fprintf out_ch "sent_id\tleft_context\tpivot\tright_context\n"
    end;
    Array.iter (fun result ->
        Array.iter (fun {corpus_index; sent_id; filename; list_item; matching} ->
            let graph = Corpus.get_graph corpus_index corpus in
            let sentence =
              match pivot with
              | None -> Graph.to_sentence graph
              | Some _ ->
                let deco = Deco.build session.pattern matching in
                Graph.to_sentence ?pivot ~deco graph
                |> (Str.global_replace (Str.regexp_string "<span class=\"highlight\">") "\t")
                |> (Str.global_replace (Str.regexp_string "</span>") "\t") in
            fprintf out_ch "%s\t%s\n" sent_id sentence
          ) result.data
      ) session.results;
    close_out out_ch

  let conll uuid string_position string_cluster=
    try
      let session = String_map.find uuid !Client_map.t in
      let config = Corpus_desc.get_config session.corpus_desc in
      let position = int_of_string string_position in
      let cluster = int_of_string string_cluster in
      let result = session.Session.results.(cluster) in
      let occ = result.Session.data.(position) in
      let corpus = Table.get_index session.index in
      let gr = Corpus.get_graph occ.Session.corpus_index corpus in
      gr |> Graph.to_json |> Conllx.of_json |> (Conllx.to_string ~config)
    with Not_found -> raise Not_connected

  let parallel uuid corpus_id sent_id =
    let (_,corpus,corpus_desc) = Table.get_name corpus_id in
    let config = Corpus_desc.get_config corpus_desc in
    match Corpus.graph_of_sent_id sent_id corpus with
    | None -> raise (Unknown_sent_id sent_id)
    | Some graph ->
      match Corpus.is_conll corpus with
      | true ->
        let dep = Graph.to_dep ~filter ~config graph in
        let d2p =
          try Dep2pict.from_dep ~rtl:(Corpus_desc.is_rtl corpus_desc) ~dep
          with Dep2pict.Error json ->
            let message =
              match json |> Yojson.Basic.Util.member "message" |> Yojson.Basic.Util.to_string_option with
              | None -> Yojson.Basic.to_string json
              | Some s -> s in
            raise (Dep2pict_error (uuid, sprintf "[sent_id=%s] %s" sent_id message)) in
        let filename = sprintf "%04x%04x.svg" (Random.int 0xFFFF) (Random.int 0xFFFF) in
        let _ = Dep2pict.save_svg ~filename:(Filename.concat uuid filename) d2p in
        Filename.concat (Filename.basename uuid) filename
      | false -> "TAGADA--dot"

  let reply local_socket msg =
    printf "==> %s\n%!" msg;
    Utils.send_string local_socket (Bytes.of_string msg);
    Unix.close local_socket

  let handle local_socket request =
    printf "==> handle request = \"%s\"\n%!" request;
    Client_map.purge ();
    match Str.split (Str.regexp "#") request with
    | [uuid; "NEW"] ->
      begin
        try
          add_new uuid;
          reply local_socket "OK"
        with exc -> error_handle uuid exc; reply local_socket "ERROR"
      end
    | [uuid; "NEXT"; index] ->
      begin
        try
          next uuid (int_of_string index);
          reply local_socket "OK"
        with exc -> error_handle uuid exc; reply local_socket "ERROR"
      end
    | [uuid; "EXPORT"; pivot] ->
      begin
        try
          export ~pivot uuid;
          reply local_socket "OK"
        with exc -> error_handle uuid exc; reply local_socket "ERROR"
      end
    | [uuid; "EXPORT"] ->
      begin
        try
          export uuid;
          reply local_socket "OK"
        with exc -> error_handle uuid exc; reply local_socket "ERROR"
      end
    | [uuid; "CONLL"; position; cluster] ->
      begin
        try
          let out = conll uuid position cluster in
          reply local_socket out
        with exc -> error_handle uuid exc; reply local_socket "ERROR"
      end
    | [uuid; "PARALLEL"; corpus_id; sent_id] ->
      begin
        try
          let out = parallel uuid corpus_id sent_id in
          reply local_socket out
        with exc -> error_handle uuid exc; reply local_socket "ERROR"
      end
    | _ -> reply local_socket (sprintf "Cannot understand request >>>%s<<<\n%!" request)
end (* module Request *)
