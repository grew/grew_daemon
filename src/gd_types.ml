open Printf
open Libgrew
open Conllx

open Gd_utils
open Gd_global

(* ==================================================================================================== *)
(* If the same user requests for several patterns there will be several [Session] for the daemon *)
module Session = struct
  type occurrence = {
    corpus_index: int;    (* graph position in the corpus array *)
    sent_id: string;
    filename: string;     (* filename used for image storing *)
    list_item: string;    (* text to put in the occurences list *)
    matching: Matching.t; (* matching data *)
  }
  type sub_result = {
    size: int;
    data: occurrence array;
    next: int;               (* position of the next solution to produce *)
    key: string option;      (* cluster key *)
  }
  type t = {
    index: int;                (* index of the corpus *)
    last: float;               (* Unix.gettimeofday of the last interaction time *)
    pattern: Pattern.t;        (* the requested pattern *)
    results: sub_result array; (* all computed solutions *)
    corpus_desc: Corpus_desc.t;
  }
end

(* ==================================================================================================== *)
module Table = struct
  exception No_marshal_file of string

  (* let (t : Corpus.t option array) = Array.make Global.max_size None *)
  let (t : (Corpus.t * float) option array) = Array.make Global.max_size None

  let dump () =
    Array.iter (function None -> printf "N" | _ -> printf "S") t;
    printf " <====== table ======\n%!"

  let update () =
    printf "update\n%!";
    dump ();
    for i = 0 to Global.max_size - 1 do
      match t.(i) with
      | Some (_,ts) when Unix.gettimeofday () -. ts > Global.timeout -> t.(i) <- None
      | _ -> ()
    done;
    dump ();
    Gc.major ()

  let get_index index =
    match t.(index) with
    | Some (data,_) ->
      t.(index) <- Some (data, Unix.gettimeofday ());
      data
    | None ->
      let corpus_desc = List.nth !Global.corpus_list index in
      match Corpus_desc.load_corpus_opt corpus_desc with
      | None -> raise (No_marshal_file (Corpus_desc.get_id corpus_desc))
      | Some data ->
        t.(index) <- Some (data, Unix.gettimeofday ());
        dump ();
        data

  let get_name corpus_name =
    printf "[Table.get]\n%!";
    dump ();
    let (index, corpus_desc) =
      try List_.find_index (fun x -> (Corpus_desc.get_id x) = corpus_name) !Global.corpus_list
      with Not_found -> raise (Unknown_corpus corpus_name) in
    match t.(index) with
    | Some (data,_) ->
      t.(index) <- Some (data, Unix.gettimeofday ());
      (index, data, corpus_desc)
    | None ->
      match Corpus_desc.load_corpus_opt corpus_desc with
      | None -> raise (No_marshal_file (Corpus_desc.get_id corpus_desc))
      | Some data ->
        t.(index) <- Some (data, Unix.gettimeofday ());
        dump ();
        (index, data, corpus_desc)
end

(* ==================================================================================================== *)
module Client_map = struct
  let (t : Session.t String_map.t ref) = ref (String_map.empty)

  let dump_client_map () =
    printf "------------- <client map> -----------------\n";
    String_map.iter (fun id _ -> printf "[%s]\n" id) !t;
    printf "------------- </client map> ----------------\n"

  (* remove expired items from client_map *)
  let purge () =
    t :=
      String_map.filter
        (fun _ ({Session.last}) -> Unix.gettimeofday () -. last < Global.timeout)
        !t;
    Table.update ();
end
