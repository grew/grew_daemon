open Printf
open Libamr

exception Already_connected
exception Not_connected
exception Dep2pict_error of (string * string)
exception Unknown_corpus of string
exception Unknown_sent_id of string

(* ==================================================================================================== *)
module String_map : Map.S with type key = string

(* ==================================================================================================== *)
module List_ : sig
  val foldi_right: (int -> 'b -> 'a -> 'a) -> 'b list -> 'a -> 'a

  val find_index: ('a -> bool) -> 'a list -> (int * 'a)
end

(* ==================================================================================================== *)
module Array_ : sig
  val foldi_left: (int -> 'a -> 'b -> 'a) -> 'a -> 'b array -> 'a

  val shuffle_N: int -> int array
end

(* ==================================================================================================== *)
module Utils : sig
  val sock_recv: Unix.file_descr -> int -> bytes

  val send_string: Unix.file_descr -> bytes -> unit

  val fill: string -> string -> string -> unit

  val fill_list: string -> string -> unit

  val error: string -> string -> unit

  val read_file: string -> string list

  val read_first_line: string -> string

  val print_with_lines: string -> unit

  val read_dir: string list -> string -> string list
end (* module Utils *)
