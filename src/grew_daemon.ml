open Printf
open Log

open Libamr

open Libgrew
open Dep2pict

open Gd_utils
open Gd_global
open Gd_types
open Gd_request


(* ==================================================================================================== *)
let main () =
  Log.set_active_levels [`INFO; `MESSAGE; `WARNING];
  let () = Global.parse_arg () in

  match !Global.mode with
  | Global.Help -> assert false
  | Global.Run ->

    (* create the socket *)
    let _ = printf "--> create a socket ... %!" in
    let socket = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
    let _ = printf "--> OK\n%!" in

    (* bind the socket to the host machine on the given port *)
    let _ = printf "--> bind the socket on port %d ... %!" !Global.port in
    let () = Unix.bind socket (Unix.ADDR_INET (Unix.inet_addr_loopback, !Global.port)) in
    let _ = printf "--> OK\n%!" in

    (* let the socket listen *)
    let _ = printf "--> start listening on the socket ...%!" in
    let () = Unix.listen socket max_int in
    let _ = printf "--> OK\n%!" in

    let m = Mutex.create () in

    while true do
      let _ = printf "--> ready ...%!" in
      let (local_socket, _) = Unix.accept socket in
      let _ = printf "accepted ...%!" in
      let request = Bytes.to_string (Utils.sock_recv local_socket 4096) in
      let _ = printf "--> OK\n%!" in

      Mutex.lock m;
      Request.handle local_socket request;
      Mutex.unlock m;
    done

let _ = main ()
