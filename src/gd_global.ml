open Printf

open Log
open Dep2pict
open Libgrew

open Gd_utils

module Global = struct
  let timeout = 300.
  let nbre_sol_page = 10
  let max_size = 700
  let max_results = 1000
  let max_time = 10.   (* time bound for a request *)
  type mode = Help | Run
  let mode = ref Help

  let webserver_dir = ref None

  let port = ref 8080

  let patterns = ref []

  let corpus_list = ref []

  let usage () = List.iter (fun l -> printf "%s\n" l) [
      "Usages: ";
      " - grew_daemon run [--port port_number] <corpus_list>    run the daemon";
      " - grew_daemon marshal <corpus_list>                     update marshal files";
      "";
      "Parameter:";
      " -p | --port  <port number>        set the port use (default is 8080)";
      " --webserver  <directory>          produces tables of relations and logs for the webserver";
      "";
      " --patterns  <files>      describes the patterns";
      " --pattern_dir  <directory>      the directory containing patterns to use";
      "";
      "Note: see https://gitlab.inria.fr/grew/grew_daemon for <corpus_list> file format";
      "=-=-=-=-=-=-=-=-=-=-=-=-=";
      "version="^VERSION;
    ]; exit 0

  let parse_arg () =
    let rec loop = function
      | [] -> ()

      | "-p" :: n :: tail
      | "--port" :: n :: tail -> port := int_of_string n; loop tail

      | "--debug" :: tail -> Libgrew.set_debug_mode true; Dep2pict.set_verbose () ; loop tail
      | "--special_chars" :: f :: tail -> Dep2pict.load_special_chars f; loop tail

      | "--webserver"::f:: tail -> webserver_dir := Some f; loop tail

      | "--patterns"::d:: tail -> patterns := Str.split (Str.regexp " +") d; loop tail
      | "--pattern_dir"::d:: tail -> patterns := List.map (fun f -> Filename.concat d f) (Utils.read_dir [".pat"] d); loop tail

      | s:: tail when s.[0] = '-' -> Log.fwarning "unknown option %s, skip it" s; loop tail

      | filename :: tail -> corpus_list := (Corpus_desc.load_json filename) @ !corpus_list; loop tail in

    begin
      match Array.to_list Sys.argv with
      | [] -> assert false
      | [_]
      | _::"help"::_ -> usage ()
      | _::"run"::args -> mode := Run; loop args
      | _::s::_ -> Utils.print_with_lines ("Unknown mode: "^s); usage ()
    end;

    match !corpus_list with
    | [] -> Utils.print_with_lines "Error: a corpus list is required"; usage ()
    | _ -> ()
end (* module Global *)
