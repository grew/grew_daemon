open Gd_utils
open Gd_global
open Conllx

open Libgrew


(* ==================================================================================================== *)
(* If the same user requests for several patterns there will be several [Session] for the daemon *)
module Session : sig
  type occurrence = {
    corpus_index: int;    (* graph position in the corpus array *)
    sent_id: string;
    filename: string;     (* filename used for image storing *)
    list_item: string;    (* text to put in the occurences list *)
    matching: Matching.t; (* matching data *)
  }
  type sub_result = {
    size: int;
    data: occurrence array;
    next: int;               (* position of the next solution to produce *)
    key: string option;      (* cluster key *)
  }
  type t = {
    index: int;                (* index of the corpus *)
    last: float;               (* Unix.gettimeofday of the last interaction time *)
    pattern: Pattern.t;        (* the requested pattern *)
    results: sub_result array; (* all computed solutions *)
    corpus_desc: Corpus_desc.t;
  }
end

(* ==================================================================================================== *)
module Table : sig
  exception No_marshal_file of string

  val t : (Corpus.t * float) option array

  val get_index: int -> Corpus.t
  val get_name: string -> (int * Corpus.t * Corpus_desc.t)

end

(* ==================================================================================================== *)
module Client_map : sig
  val t: Session.t String_map.t ref

  val purge: unit -> unit
end
