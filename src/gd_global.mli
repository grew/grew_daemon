open Printf

open Dep2pict
open Libgrew

open Gd_utils

module Global : sig
  (* Size of the table storing loaded corpora *)
  val max_size: int

  val port: int ref

  (* Maximum of inactive time between two interaction on the same resquets *)
  val timeout: float

  val nbre_sol_page: int

  val max_results: int

  val max_time: float

  val webserver_dir: string option ref

  (* OUT *)
  val patterns: string list ref

  type mode = Help | Run

  val mode: mode ref

  val corpus_list: Corpus_desc.t list ref

  val parse_arg: unit -> unit
end (* module Global *)
