PREFIX?=/usr/local
BINDIR=$(PREFIX)/bin

OCB_FLAGS = -use-ocamlfind -I src
OCB = ocamlbuild $(OCB_FLAGS)

VERSION = `cat VERSION`

all: native

native:
	$(OCB) -tag-line "true:	package(libgrew)" grew_daemon.native

byte:
	$(OCB) -tag-line "true:	package(libgrew)" grew_daemon.byte

install: native
	cp grew_daemon.native $(BINDIR)/grew_daemon

uninstall:
	rm -f $(BINDIR)/grew_daemon

.PHONY:	all clean byte native install uninstall

clean:
	$(OCB) -clean

info:
	@echo "BINDIR   = $(BINDIR)"

